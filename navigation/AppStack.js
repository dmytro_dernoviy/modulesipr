import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import RecordingAudioScreen from '../screens/RecordingAudioScreen';
import AudioListScreen from '../screens/AudioListScreen';
import HooksTraining from '../screens/HooksTraining';
import InAppPurchase from '../screens/InAppPurchase';
import FileViewScreen from '../screens/FileViewScreen';
import VideoScreen from '../screens/VideoScreen';
import SelectChart from '../screens/charts/SelectChart';
import LineChartScreen from '../screens/charts/LineChartScreen';
import PieChartScreen from '../screens/charts/PieChartScreen';
import BarChartScreen from '../screens/charts/BarChartScreen';
import HorizontalBarChartScreen from '../screens/charts/HorizontalBarChartScreen';
import HighChartScreen from '../screens/charts/HighChart';

const AppStack = createStackNavigator(
  {
    RecordingAudio: { screen: RecordingAudioScreen },
    AudioList: { screen: AudioListScreen },
    HooksTrainig: { screen: HooksTraining },
    InAppPurchase: { screen: InAppPurchase },
    FileViewScreen: { screen: FileViewScreen },
    VideoScreen: { screen: VideoScreen },
    SelectChart: { screen: SelectChart },
    LineChart: { screen: LineChartScreen },
    PieChart: { screen: PieChartScreen },
    BarChart: { screen: BarChartScreen },
    HorizontalBarChart: { screen: HorizontalBarChartScreen },
    HighChart: { screen: HighChartScreen },
  },
  {
    initialRouteName: 'SelectChart',
    headerMode: 'none',
  },
);

export default createAppContainer(AppStack);
