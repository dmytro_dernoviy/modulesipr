export const getLastAudioIndex = array => {
  const arrayOfIndex = array.map(item => parseInt(item.name.slice(4, 5), 10));
  return Math.max.apply(null, arrayOfIndex);
};
