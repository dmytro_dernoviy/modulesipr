export const convertURI = uri => {
  const id = uri?.substring(5, 41);
  const ext = 'mov';
  return `assets-library://asset/asset.${ext}?id=${id}&ext=${ext}`;
};
