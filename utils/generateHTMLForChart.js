import { Dimensions } from 'react-native';
let highcharts = require('highcharts');

const { height } = Dimensions.get('window');

export const generateHTMLForChart = yAxisData => {
  const yData = JSON.stringify(yAxisData);
  const html = /*html*/ `<!DOCTYPE html>
                    <html>
                    <head>
                      <script>${highcharts}</script>
                      <meta name="viewport" content="width=device-width, initial-scale=0.9, user-scalable=0">
                      <style type="text/css">
                        .highcharts-grid path:first-child {
                            display: none;
                        }
                      </style>
                    </head>
                    <body>
                      <div id="container" style="width: 100%; height: 100%">
                      </div>
                         <script type="text/javascript">
                            const yAxisData = JSON.parse('${yData}');
                            const Highcharts = new ${highcharts}();
                            
                            Highcharts.chart('container', {
                                chart: {
                                            type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        },
                                    spacingTop: 0,
                                    spacingBottom: 0,
                                    spacingRight: 0,
                                    spacingLeft: 0,
                                    backgroundColor: 'transparent',
                                    showAxes: false,
                                    height: ${height} * 0.5,
                                },
                                credits: {
                                    enabled: false,
                                },
                                title: {
                                    text: 'Fruit Consumption',
                                    style: {
                                        "color": "white",
                                        "font-size": "20px"
                                    }
                                },
                                xAxis: {
                                    startOnTick: false,
                                    lineColor: 'transparent',
                                    reversed: true,
                                    gridLineWidth: 1,
                                    gridLineColor: 'lightgreen',
                                    labels: {
                                        enabled: false
                                    },
                                    plotLines: [{ 
                                        color: 'red',
                                    }],
                                },
                                yAxis: {
                                    offset: 0,
                                    startOnTick: false,
                                    opposite: true,
                                    lineWidth: 0,
                                    title: {
                                        enabled: false
                                    },
                                    labels: {
                                        align: "left",
                                        style: {
                                            "color": "white",
                                            "font-size": "20px"
                                        }
                                    },
                                    gridLineWidth: 0,
                                
                                },
                                series: [{
                                    name: 'Jane',
                                    showInLegend: false,
                                    data: yAxisData,
                                    color: 'lightgreen',
                                    marker: {
                                        enabled: false
                                    },
                                    fillColor : {
                                        linearGradient : {
                                            y1: 0,
                                            y2: 1,
                                        },
                                        stops : [
                                            [0, Highcharts.Color('black').setOpacity(1).get('rgba')],
                                            [1, Highcharts.Color('rgba(26, 182, 151, 0.5)').setOpacity(0).get('rgba')],
                                        ]
                                    }
                                }]
                            });
                        </script>
                    </body>
                    </html>`;
  return html;
};
