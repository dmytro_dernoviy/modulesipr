import React from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const ChartWrapper = ({ navigation, children }) => {
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={() => navigation.goBack(null)}>
        <Icon name="arrow-back" color="white" size={50} />
      </TouchableOpacity>
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(34, 43, 48)',
  },
});

export default ChartWrapper;
