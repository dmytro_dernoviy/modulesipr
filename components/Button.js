import React, { memo } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Button = memo(props => {
  console.log('render button');
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Click</Text>
    </View>
  );
});

const styles = StyleSheet.create({
  label: {
    fontSize: 40,
  },
  container: {
    backgroundColor: 'lightgreen',
    color: 'black',
  },
});

export default Button;
