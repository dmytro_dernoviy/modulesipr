import React, { useState, useEffect } from 'react';
import { Modal } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import RNConvertPhAsset from 'react-native-convert-ph-asset';

const VideoPlayerComponent = ({ visible, togglePlayer, uri }) => {
  const [videoUri, setVideoUri] = useState('');
  useEffect(() => {
    RNConvertPhAsset.convertVideoFromUrl({
      url: uri || '',
      convertTo: 'mov',
      quality: 'original',
    })
      .then(response => {
        setVideoUri(response.path);
      })
      .catch(err => {
        console.log(err);
      });
    return () => {
      setVideoUri('');
    };
  }, [uri]);

  return (
    <Modal
      visible={visible}
      animationType="slide"
      presentationStyle="fullScreen"
      supportedOrientations={['landscape', 'portrait']}>
      <VideoPlayer
        toggleResizeModeOnFullscreen
        source={{
          uri: videoUri,
        }}
        onBack={togglePlayer}
      />
    </Modal>
  );
};

export default VideoPlayerComponent;
