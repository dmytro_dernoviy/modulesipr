import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const RecordedAudio = ({title, isActive}) => {
  const optinalStyles = {
    backgroundColor: isActive ? 'black' : '#696969',
  };

  return (
    <View
      style={{
        ...styles.audioContainer,
        ...optinalStyles,
      }}>
      <Text style={styles.audioTitle} ellipsizeMode="tail" numberOfLines={1}>
        {title}
      </Text>
      <Icon name="audiotrack" color="white" size={60} style={styles.icon} />
    </View>
  );
};

const styles = StyleSheet.create({
  audioContainer: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10,
    borderRadius: 5,
  },
  audioTitle: {
    fontSize: 40,
    color: 'white',
  },
  icon: {
    position: 'absolute',
    right: 0,
  },
});

export default RecordedAudio;
