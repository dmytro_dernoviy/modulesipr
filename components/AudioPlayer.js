import React, { useState, useEffect, useCallback } from 'react';
import {
  TouchableWithoutFeedback,
  StyleSheet,
  View,
  TouchableOpacity,
} from 'react-native';
import Slider from '@react-native-community/slider';
import AudioPlayingService from '../services/AudioPlayingService';
import Icon from 'react-native-vector-icons/MaterialIcons';

export let audioTime;

const AudioPlayer = ({ audioPath }) => {
  const [isPlaying, setPlaying] = useState(false);
  const [currentTime, setCurrentTime] = useState(0);
  const [durationTime, setDurationTime] = useState(0);
  const [isEditingPlayer, setEditingPlayer] = useState(false);

  useEffect(() => {
    startPlaying();
  }, [audioPath, startPlaying, isEditingPlayer]);

  const startPlaying = useCallback(() => {
    if (!audioPath) {
      return;
    }

    AudioPlayingService.startPlaying(audioPath, setPlaying, audioPlayerRelease);

    audioTime = setInterval(() => {
      if (
        AudioPlayingService.sound &&
        AudioPlayingService.sound.isLoaded() &&
        !isEditingPlayer
      ) {
        AudioPlayingService.getCurrentTime(setCurrentTime);
        setDurationTime(AudioPlayingService.getDuration());
      }
    }, 20);

    setPlaying(true);
  }, [audioPath, isEditingPlayer]);

  const pausePlaying = () => {
    AudioPlayingService.pausePlaying();
    setPlaying(false);
  };

  const audioPlayerRelease = () => {
    clearInterval(audioTime);
  };

  const onStopPlayingCallback = () => {
    setPlaying(false);
    audioPlayerRelease();
  };

  const onPlayerEditStart = () => {
    setEditingPlayer(true);
  };

  const onPlayerEditEnd = value => {
    setEditingPlayer(false);
  };

  const onChangePlayer = value => {
    AudioPlayingService.setCurrentTime(value);
  };

  return (
    <View style={styles.container}>
      {!isPlaying || isEditingPlayer ? (
        <TouchableOpacity onPress={() => startPlaying()}>
          <Icon name="play-arrow" color="black" size={45} />
        </TouchableOpacity>
      ) : (
        <TouchableWithoutFeedback onPress={() => pausePlaying()}>
          <Icon name="pause" color="white" size={45} />
        </TouchableWithoutFeedback>
      )}
      <Slider
        step={0.1}
        maximumValue={durationTime}
        value={currentTime}
        style={styles.slider}
        maximumTrackTintColor="black"
        onValueChange={onChangePlayer}
        onSlidingStart={onPlayerEditStart}
        onSlidingComplete={onPlayerEditEnd}
      />
      <TouchableWithoutFeedback
        onPress={() => AudioPlayingService.stopPlaying(onStopPlayingCallback)}>
        <Icon name="stop" color="black" size={45} />
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  text: {
    fontSize: 30,
    color: 'white',
  },
  slider: {
    width: '70%',
  },
});

export default AudioPlayer;
