import RNFS from 'react-native-fs';
import { Alert } from 'react-native';
import { getLastAudioIndex } from '../utils/getLastAudioIndex';
import RNBackgroundDownloader from 'react-native-background-downloader';

class FSService {
  constructor() {
    this.listOfDirectories = null;
    this.uploadURL = 'https://content.dropboxapi.com/2/files/upload';
  }
  getListOfDirectories = async () => {
    try {
      const result = await RNFS.readDir(
        RNBackgroundDownloader.directories.documents,
      );
      this.listOfDirectories = result;
      console.log(RNFS.DocumentDirectoryPath);
      return result;
    } catch (error) {
      console.log(error);
    }
  };

  createFile = async () => {
    let path = '';
    try {
      const response = await RNFS.writeFile(
        path,
        'Lorem ipsum dolor sit amet',
        'utf8',
      );
      console.log('file has been created', response);
    } catch (error) {
      console.log(error);
    }
  };

  uploadBegin = response => {
    const jobId = response.jobId;
    console.log('start uploading with jobId', jobId);
  };

  uploadProgress = response => {
    let percentage = Math.floor(
      (response.totalBytesSent / response.totalBytesExpectedToSend) * 100,
    );
    console.log('UPLOAD IS ' + percentage + '% DONE!');
  };

  uploadFiles = async file => {
    const files = Array.from([
      {
        filename: file.name,
        filepath: file.path,
      },
    ]);

    try {
      await RNFS.uploadFiles({
        toUrl: this.uploadURL,
        files: files,
        method: 'POST',
        headers: {
          Authorization:
            'Bearer WSWT-EIfkwAAAAAAAAAAIaA-zCDu8NJJS_PlrlL8HAqPbMKrj8dZD4uEM5wv4YGB',
          'Content-Type': 'application/octet-stream',
          'Dropbox-API-Arg': JSON.stringify({
            path: `/${file.name}`,
            mode: 'add',
            autorename: true,
            mute: false,
          }),
        },
        begin: this.uploadBegin,
        progress: this.uploadProgress,
      }).promise;

      Alert.alert('Upload succesfully');
    } catch (error) {
      console.log(error);
    }
  };

  getNameNextRecording = () => {
    if (this.listOfDirectories.length === 0) {
      return '/test1.aac';
    }
    this.getListOfDirectories();
    const lastIndex = getLastAudioIndex(this.listOfDirectories);
    console.log(lastIndex);
    return `/test${lastIndex + 1}.aac`;
  };

  deleteFile = async path => {
    try {
      await RNFS.unlink(path);
      console.log('file has been deleted', path);
    } catch (error) {
      console.log(error);
    }
  };

  mkDir = async dirName => {
    try {
      const newFolderPath = RNFS.DocumentDirectoryPath + `/${dirName}`;
      await RNFS.mkdir(newFolderPath);
      console.log(`${dirName} directory has been created successfully`);
    } catch (error) {
      console.log(error);
    }
  };
}

export default new FSService();
