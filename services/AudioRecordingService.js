import {AudioRecorder, AudioUtils} from 'react-native-audio';
import {Platform} from 'react-native';
import FSService from './FSService';

class AudioRecordingService {
  constructor() {
    this.audioPath = null;
    this.hasPermission = null;
    this.setStateCallback = undefined;
    this.getState = null;
    this.audioFileURL = null;
  }

  prepareRecordingPath = audioPath => {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: 'High',
      AudioEncoding: 'aac',
      AudioEncodingBitRate: 32000,
    });
  };

  init = async (setStateCallback, getState) => {
    this.setStateCallback = setStateCallback;
    this.getState = getState;

    try {
      const isAuthorized = await AudioRecorder.requestAuthorization();
      this.hasPermission = isAuthorized;

      if (!isAuthorized) {
        return;
      }

      this.setOnProgress();
      this.setOnFinish();
    } catch (error) {
      console.log('Audio recorder authorization error', error);
    }
  };

  setOnProgress = () => {
    AudioRecorder.onProgress = data => {
      this.setStateCallback({
        type: 'setCurrentTime',
        payload: data.currentTime.toFixed(2),
      });
    };
  };

  setOnFinish = () => {
    AudioRecorder.onFinished = data => {
      if (Platform.OS === 'ios') {
        this.finishRecording(
          data.status === 'OK',
          data.audioFileURL,
          data.audioFileSize,
        );
      }
    };
  };

  startRecord = async () => {
    const nextAudioName = FSService.getNameNextRecording();
    this.audioPath = AudioUtils.DocumentDirectoryPath + nextAudioName;
    this.prepareRecordingPath(this.audioPath);
    this.setStateCallback({type: 'startRecording'});
    try {
      await AudioRecorder.startRecording();
    } catch (error) {
      console.log(error);
    }
  };

  pauseRecord = async () => {
    const {recording} = this.getState();
    if (!recording) {
      return console.log('not recording');
    }
    try {
      await AudioRecorder.pauseRecording();
      this.setStateCallback({type: 'pauseRecord'});
    } catch (error) {
      console.log(error);
    }
  };

  resumeRecord = async () => {
    try {
      await AudioRecorder.resumeRecording();
      this.setStateCallback({type: 'resumeRecord'});
    } catch (error) {
      console.log(error);
    }
  };

  stopRecording = async () => {
    this.setStateCallback({type: 'stopRecording'});
    try {
      await AudioRecorder.stopRecording();
    } catch (error) {
      console.error(error);
    }
  };

  finishRecording = (didSucceed, filePath, fileSize) => {
    this.setStateCallback({type: 'finishRecording', payload: didSucceed});
    this.audioFileURL = filePath;
  };
}

export default new AudioRecordingService();
