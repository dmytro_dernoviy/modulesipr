import FileViewer from 'react-native-file-viewer';
import OpenFile from 'react-native-doc-viewer';
import { Platform } from 'react-native';

class FileViewService {
  constructor() {
    this.filePath = null;
  }

  open = async path => {
    try {
      await FileViewer.open(path);
    } catch (error) {
      console.log(error);
    }
  };

  openByDocViewer = (path, type) => {
    if (Platform.OS === 'ios') {
      OpenFile.openDoc(
        [
          {
            url: path,
            fileNameOptional: 'test filename',
          },
        ],
        (error, url) => {
          if (error) {
            console.log('doc error', error);
          } else {
            console.log('doc url', url);
          }
        },
      );
    } else {
      OpenFile.openDoc(
        [
          {
            url: 'file://' + path,
            fileName: 'sample',
            cache: false,
            fileType: 'docx',
          },
        ],
        (error, url) => {
          if (error) {
            console.log('doc error android', error);
          } else {
            console.log('doc url android', url);
          }
        },
      );
    }
  };
}

export default new FileViewService();
