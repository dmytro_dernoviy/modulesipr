import stripe from 'tipsi-stripe';
import { create } from 'apisauce';

class StripeService {
  doPayment = async token => {
    const requestHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };

    const requestBody = {
      amount: 12300,
      currency: 'usd',
      token: token,
    };

    const api = create({
      baseURL: 'http://localhost:5005/stripetesting-e8c09/us-central1',
    });

    try {
      const response = await api.post(
        '/payWithStripe',
        requestBody,
        requestHeaders,
      );
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  paymentRequestByCard = async () => {
    stripe.setOptions({
      publishableKey: 'pk_test_EfjoDykPuc8s1CrbGQDRjRmR00DVw3GqWo',
    });
    try {
      const { tokenId } = await stripe.paymentRequestWithCardForm({
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          billingAddress: {
            name: 'Enappd Store',
            line1: 'Canary Place',
            line2: '3',
            city: 'Macon',
            state: '',
            country: 'Estonia',
            postalCode: '31217',
            email: 'admin@enappd.com',
          },
        },
      });
      this.doPayment(tokenId);
    } catch (error) {
      console.log(error);
    }
  };
}

export default new StripeService();
