import RNBackgroundDownloader from 'react-native-background-downloader';

class BackgroundDownloadService {
  constructor() {
    this.task = null;
    this.downloadLink =
      'https://example-files.online-convert.com/document/docx/example.docx';
    this.observers = [];
  }

  subscribe = callback => {
    this.observers.push(callback);
  };

  unsubscribe = callback => {
    this.observers = this.observers.filter(
      subscriber => callback !== subscriber,
    );
  };

  downloadBegin = expectedBytes => {
    console.log(`Going to download ${expectedBytes} bytes!`);
  };

  downloadProgress = percent => {
    if (this.observers.length !== 0) {
      this.observers.forEach(subcriber => subcriber(percent));
    }
  };

  downloadPause = () => {
    this.task.pause();
    console.log(this.task);
  };

  downloadFinished = () => {
    if (this.observers.length !== 0) {
      this.observers.forEach(subcriber => subcriber('done'));
    }
    console.log('finished download');
  };

  downloadError = error => {
    console.log(`Download error ${error}`);
  };

  startDownload = () => {
    this.task = RNBackgroundDownloader.download({
      id: 'example.docx',
      url: this.downloadLink,
      destination: `${
        RNBackgroundDownloader.directories.documents
      }/example.docx`,
    })
      .begin(this.downloadBegin)
      .progress(this.downloadProgress)
      .done(this.downloadFinished)
      .error(this.downloadError);
  };

  reAttachTasks = async () => {
    try {
      const lostTasks = await RNBackgroundDownloader.checkForExistingDownloads();
      console.log(lostTasks);
      for (let task of lostTasks) {
        console.log(`Task ${task.id} was found!`);
        task.progress(percent => {
          console.log(`Downloaded: ${percent * 100}%`);
        });
      }
    } catch (error) {
      console.log('Download canceled due to error: ', error);
    }
  };
}

export default new BackgroundDownloadService();
