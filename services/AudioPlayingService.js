import Sound from 'react-native-sound';

class AudioPlayingService {
  constructor() {
    this.sound = null;
  }

  startPlaying = (audioPath, callback, audioPlayerRelease) => {
    if (!audioPath) {
      return;
    }

    if (this.sound) {
      this.sound.play(success => {
        callback(false);
        audioPlayerRelease();
      });
    } else {
      this.sound = new Sound(audioPath, '', error => {
        if (error) {
          console.log(error);
        }

        this.sound.play(success => {
          callback(false);
          audioPlayerRelease();
        });
      });
    }
  };

  getCurrentTime = callback => {
    this.sound.getCurrentTime(seconds => {
      callback(seconds);
    });
  };

  getDuration = () => {
    return this.sound.getDuration();
  };

  setCurrentTime = currentTime => {
    if (this.sound) {
      this.sound.setCurrentTime(currentTime);
    }
  };

  pausePlaying = callback => {
    this.sound.pause();
  };

  stopPlaying = callback => {
    if (this.sound) {
      this.sound.stop(() => {
        callback();
      });
    }
  };

  release = () => {
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
  };
}

export default new AudioPlayingService();
