import BraintreeDropIn from 'react-native-braintree-dropin-ui';
import {requestOneTimePayment} from 'react-native-paypal';

class BraintreeService {
  openBraintreeDropIn = async () => {
    try {
      const response = await BraintreeDropIn.show({
        clientToken: 'sandbox_hc6vkytt_gkzptzsry2k7w2y7',
        vaultManager: false,
        orderTotal: 25,
      });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  payPalRequest = async () => {
    const result = await requestOneTimePayment(
      'sandbox_hc6vkytt_gkzptzsry2k7w2y7',
      {
        amount: '5',
        currency: 'USD',
        localeCode: 'en_US',
        shippingAddressRequired: false,
        userAction: 'commit',
        intent: 'authorize',
      },
    );
    console.log(result);
  };
}

export default new BraintreeService();
