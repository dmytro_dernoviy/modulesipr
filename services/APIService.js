import { create } from 'apisauce';
import { apiConsts } from '../constants/apiConsts';

class APIService {
  constructor() {
    this.URL = apiConsts.baseURL;
    this.API = create({ baseURL: this.URL });
  }

  getProducts = () => {
    return this.API.get(apiConsts.getProducts);
  };
}

export default new APIService();
