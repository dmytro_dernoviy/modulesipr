import {combineReducers} from 'redux';
import getProductsReducer from './getProducts/reducer';

const rootReducer = combineReducers({
  getProductsReducer,
});

export default rootReducer;
