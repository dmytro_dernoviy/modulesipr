import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './rootReducer';
import {rootSaga} from './rootSaga';

const sagaMiddleWare = createSagaMiddleware();
const middleWares = [sagaMiddleWare, logger];

export const store = createStore(rootReducer, applyMiddleware(...middleWares));
sagaMiddleWare.run(rootSaga);
