import {all} from 'redux-saga/effects';
import {watchGetPosters} from './getProducts/sagas';

export function* rootSaga() {
  yield all([watchGetPosters()]);
}
