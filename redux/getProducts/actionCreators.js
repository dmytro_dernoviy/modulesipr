import {actionConsts} from '../../constants/actionConsts';

const getProductsActions = {
  request: () => ({type: actionConsts.GET_PRODUCTS_REQUEST}),
  success: data => ({type: actionConsts.GET_PRODUCTS_SUCCESS, data}),
  failure: data => ({type: actionConsts.GET_PRODUCTS_FAILURE, data}),
};

export default getProductsActions;
