import {put, takeEvery, call} from 'redux-saga/effects';
import APIService from '../../services/APIService';
import {actionConsts} from '../../constants/actionConsts';
import getProductsActions from './actionCreators';

function* getPostersSaga() {
  try {
    const response = yield call(APIService.getProducts);
    yield put(getProductsActions.success(response?.data));
  } catch (error) {
    yield put(getProductsActions.failure(error?.data));
  }
}

export function* watchGetPosters() {
  yield takeEvery(actionConsts.GET_PRODUCTS_REQUEST, getPostersSaga);
}
