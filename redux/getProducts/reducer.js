import {actionConsts} from '../../constants/actionConsts';

const initialState = {
  products: [],
  isLoading: false,
  error: null,
};

export default function getProductsReducer(state = initialState, action) {
  switch (action.type) {
    case actionConsts.GET_PRODUCTS_REQUEST:
      return {...state, isLoading: true, error: null};
    case actionConsts.GET_PRODUCTS_SUCCESS:
      return {...state, isLoading: false, products: action.data};
    case actionConsts.GET_PRODUCTS_FAILURE:
      return {...state, isLoading: false, error: action.data};
    default:
      return state;
  }
}
