import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import { Bar } from 'react-native-progress';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FSService from '../services/FSService';
import RecordedAudio from '../components/RecordedAudio';
import AudioPlayer, { audioTime } from '../components/AudioPlayer';
import AudioPlayingService from '../services/AudioPlayingService';
import BackgroundDownloadService from '../services/BackgroundDownloadService';

const { width } = Dimensions.get('window');

const AudioListScreen = ({ navigation }) => {
  const [activeSection, setActiveSection] = useState(null);
  const [sections, setSections] = useState([]);
  const [downloadProgress, setDownloadProgress] = useState(0);

  useEffect(() => {
    FSService.getListOfDirectories().then(response => setSections(response));
    BackgroundDownloadService.subscribe(updateDownloadProgress);

    return () => {
      BackgroundDownloadService.unsubscribe(updateDownloadProgress);
      clearInterval(audioTime);
      AudioPlayingService.release();
    };
  }, []);

  const chooseAudio = idx => {
    if (!activeSection) {
      setActiveSection(idx);
    } else if (idx !== activeSection) {
      AudioPlayingService.release();
      setActiveSection(idx);
    }
  };

  const deleteAudio = path => {
    FSService.deleteFile(path);
    setSections(prevState => prevState.filter(item => item.path !== path));
  };

  const uploadAudio = file => {
    FSService.uploadFiles(file);
  };

  const downloadFile = () => {
    BackgroundDownloadService.startDownload();
  };

  const updateDownloadProgress = progress => {
    if (progress === 'done') {
      setDownloadProgress(1);
    } else {
      setDownloadProgress(progress);
    }
  };

  const downloadPause = () => {
    BackgroundDownloadService.downloadPause();
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableWithoutFeedback onPress={() => navigation.goBack(null)}>
          <Icon name="arrow-back" color="white" size={50} />
        </TouchableWithoutFeedback>
        <Text style={styles.title}>Recording sounds</Text>
      </View>
      <AudioPlayer audioPath={sections[activeSection]?.path} />
      {sections.map((item, idx) => {
        return (
          <View style={styles.audio} key={item.name}>
            <TouchableOpacity
              style={styles.eventContainer}
              onPress={() => uploadAudio(item)}>
              <Icon name="cloud-upload" color="white" size={50} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => chooseAudio(idx)}
              style={styles.audioContainer}>
              <RecordedAudio
                title={item.name}
                isActive={idx === activeSection}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.eventContainer}
              onPress={() => deleteAudio(item.path)}>
              <Icon name="delete-forever" color="white" size={50} />
            </TouchableOpacity>
          </View>
        );
      })}
      <View style={styles.downloadControls}>
        <TouchableOpacity onPress={downloadFile}>
          <Icon name="cloud-download" color="white" size={50} />
        </TouchableOpacity>
        <TouchableOpacity onPress={downloadPause}>
          <Icon name="pause-circle-outline" color="white" size={50} />
        </TouchableOpacity>
      </View>
      <Bar
        color="lightgreen"
        progress={downloadProgress}
        width={width * 0.8}
        height={10}
        useNativeDriver
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    paddingTop: 40,
    alignItems: 'center',
  },
  audioContainer: {
    width: '67%',
  },
  title: {
    fontSize: 30,
    color: 'white',
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  audio: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
    marginBottom: 10,
  },
  eventContainer: {
    width: '15%',
    backgroundColor: '#696969',
    height: '100%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  downloadControls: {
    flexDirection: 'row',
  },
});

export default AudioListScreen;
