/* eslint-disable react-native/no-inline-styles */
import React, { useRef, useState, useCallback, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Image,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import moment from 'moment';
import { RNCamera } from 'react-native-camera';
import VideoPlayerComponent from '../components/VideoPlayer';
import { PinchGestureHandler } from 'react-native-gesture-handler';
import ZoomView from './ZoomCameraView';

const VideoScreen = () => {
  const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.back);
  const [isCameraOpen, setCameraState] = useState(false);
  const [videos, setVideos] = useState([]);
  const [isOpenPlayer, setPlayerState] = useState(false);
  const [videoUri, setVideoUri] = useState('');
  const [autoFocusPoint, setAutoFocusPoint] = useState({
    normalized: { x: 0.5, y: 0.5 },
    drawRectPosition: {
      x: Dimensions.get('window').width * 0.5 - 32,
      y: Dimensions.get('window').height * 0.5 - 32,
    },
  });
  const [zoom, setZoom] = useState(0);
  const camera = useRef('Camera');

  const toggleCameraType = useCallback(() => {
    cameraType === RNCamera.Constants.Type.back
      ? setCameraType(RNCamera.Constants.Type.front)
      : setCameraType(RNCamera.Constants.Type.back);
  }, [cameraType]);

  useEffect(() => {
    CameraRoll.getPhotos({
      first: 1,
      assetType: 'Videos',
    })
      .then(r => {
        setVideos(r.edges);
        console.log(r);
      })
      .catch(error => console.log(error));
  }, []);

  const takePicture = async () => {
    if (camera.current) {
      const options = {
        quality: RNCamera.Constants.VideoQuality['720p'],
      };
      const { uri } = await camera.current.takePictureAsync(options);
      CameraRoll.saveToCameraRoll(uri, 'photo');
    }
  };

  const startVideoRecording = async () => {
    if (camera.current) {
      const options = { quality: 1 };
      const { uri } = await camera.current.recordAsync(options);
      const uriCamera = await CameraRoll.saveToCameraRoll(uri, 'video');
      console.log(uri);
      console.log(uriCamera);
    }
  };

  const stopVideoRecording = async () => {
    if (camera.current) {
      try {
        const data = await camera.current.stopRecording();
        console.log(data.uri);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const openVideoPlayer = useCallback(
    uri => {
      setPlayerState(!isOpenPlayer);
      setVideoUri(uri);
    },
    [isOpenPlayer],
  );

  const touchToFocus = useCallback(event => {
    const { pageX, pageY } = event.nativeEvent;
    const screenWidth = Dimensions.get('window').width;
    const screenHeight = Dimensions.get('window').height;
    const isPortrait = screenHeight > screenWidth;

    let x = pageX / screenWidth;
    let y = pageY / screenHeight;
    // Coordinate transform for portrait. See autoFocusPointOfInterest in docs for more info
    if (isPortrait) {
      x = pageY / screenHeight;
      y = -(pageX / screenWidth) + 1;
    }

    setAutoFocusPoint({
      normalized: { x, y },
      drawRectPosition: { x: pageX, y: pageY },
    });
  }, []);

  const drawFocusRingPosition = {
    top: autoFocusPoint.drawRectPosition.y - 32,
    left: autoFocusPoint.drawRectPosition.x - 32,
  };

  const setGestureZoom = useCallback(value => {
    setZoom(value);
  }, []);

  const handleZoomByGesture = event => {
    const value = event.nativeEvent.scale / 250;
    setGestureZoom(value);
  };

  return (
    <SafeAreaView style={styles.container}>
      {isCameraOpen && (
        <>
          <ZoomView
            onZoomProgress={progress => {
              setGestureZoom(progress);
            }}
            onZoomStart={() => {
              console.log('zoom start');
            }}
            onZoomEnd={() => {
              console.log('zoom end');
            }}>
            <RNCamera
              zoom={zoom}
              ref={camera}
              style={styles.preview}
              type={cameraType}
              flashMode={RNCamera.Constants.FlashMode.on}
              autoFocus
              autoFocusPointOfInterest={autoFocusPoint.normalized}
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={({ barcodes }) => {
                console.log(barcodes);
              }}
            />
          </ZoomView>
          {/* <View style={StyleSheet.absoluteFill}>
            <View style={[styles.autoFocusBox, drawFocusRingPosition]} />
            <TouchableWithoutFeedback onPress={touchToFocus}>
              <View style={{ flex: 1 }} />
            </TouchableWithoutFeedback>
          </View> */}
          <View style={styles.control}>
            <TouchableOpacity onPress={takePicture} style={styles.capture}>
              <Text style={{ fontSize: 14 }}> Photo </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={startVideoRecording}
              style={styles.capture}>
              <Text style={{ fontSize: 14 }}> Video start </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={stopVideoRecording}
              style={styles.capture}>
              <Text style={{ fontSize: 14 }}> Video stop </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.topBar}>
            <TouchableOpacity
              style={[styles.capture]}
              onPress={toggleCameraType}>
              <Text style={{ fontSize: 14 }}> ToggleCamera </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.capture]}
              onPress={() => setCameraState(!isCameraOpen)}>
              <Text style={{ fontSize: 14 }}> CloseCamera </Text>
            </TouchableOpacity>
          </View>
        </>
      )}
      {!isCameraOpen && (
        <>
          <TouchableOpacity
            onPress={() => setCameraState(!isCameraOpen)}
            style={styles.cameraOpen}>
            <Text style={styles.textOpen}>Open camera</Text>
          </TouchableOpacity>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'space-between',
              padding: 10,
            }}>
            {videos.map(item => {
              const { uri, playableDuration } = item.node.image;
              return (
                <TouchableOpacity
                  style={styles.videoItem}
                  key={uri}
                  onPress={() => openVideoPlayer(uri)}>
                  <Image source={{ uri }} style={styles.image} />
                  <Text style={styles.duration}>
                    {moment(moment.duration(playableDuration) * 1000).format(
                      'mm:ss',
                    )}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </>
      )}
      <VideoPlayerComponent
        visible={isOpenPlayer}
        togglePlayer={openVideoPlayer}
        uri={videoUri}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'grey',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  control: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  toggleCamera: {
    position: 'absolute',
  },
  cameraOpen: {
    backgroundColor: 'lightgreen',
  },
  topBar: {
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
  },
  textOpen: {
    flex: 0,
    fontSize: 25,
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  duration: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    fontSize: 20,
    marginBottom: 5,
    marginRight: 5,
    color: 'white',
  },
  videoItem: {
    width: '47%',
    height: 200,
    marginBottom: 10,
  },
});

export default VideoScreen;
