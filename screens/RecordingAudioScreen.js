import React, { useEffect, useReducer } from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import AudioRecordingService from '../services/AudioRecordingService';
import FSService from '../services/FSService';

const initialState = {
  currentTime: 0.0,
  recording: false,
  paused: false,
  stoppedRecording: false,
  finished: false,
  isVisibleModal: false,
  inputValue: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'setCurrentTime':
      return { ...state, currentTime: action.payload };
    case 'startRecording':
      return { ...state, recording: true, paused: false, finished: false };
    case 'stopRecording':
      return {
        ...state,
        stoppedRecording: true,
        recording: false,
        paused: false,
      };
    case 'finishRecording':
      return { ...state, finished: action.payload };
    case 'pauseRecord':
      return { ...state, paused: true };
    case 'resumeRecord':
      return { ...state, paused: false };
    case 'toggleModal':
      return { ...state, isVisibleModal: !state.isVisibleModal };
    case 'setInputValue':
      return { ...state, inputValue: action.payload };
    default:
      throw new Error('case reducer is not matched');
  }
};

const renderButton = type => {
  const onPressEventByType = {
    'record-voice-over': () => AudioRecordingService.startRecord(),
    pause: () => AudioRecordingService.pauseRecord(),
    'play-arrow': () => AudioRecordingService.resumeRecord(),
    stop: () => AudioRecordingService.stopRecording(),
  };

  return (
    <TouchableWithoutFeedback onPress={onPressEventByType[type]}>
      <Icon name={type} color="white" size={80} />
    </TouchableWithoutFeedback>
  );
};

const RecordingAudioScreen = ({ navigation }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    FSService.getListOfDirectories();
    const getState = () => state;
    AudioRecordingService.init(setState, getState);
  }, [state]);

  const setState = ({ type, payload }) => {
    dispatch({ type, payload });
  };

  const saveAudio = () => {
    dispatch({ type: 'toggleModal' });
    console.log(state.inputValue);
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback
        onPress={() => navigation.navigate('AudioList')}>
        <Icon
          name="keyboard-arrow-right"
          size={80}
          color="white"
          style={styles.navigate}
        />
      </TouchableWithoutFeedback>
      <Modal
        visible={state.isVisibleModal}
        animationType="slide"
        transparent={true}>
        <View style={styles.backgroundModal}>
          <View style={styles.modalForm}>
            <TouchableWithoutFeedback
              onPress={() => dispatch({ type: 'toggleModal' })}>
              <Text style={styles.close}>Close</Text>
            </TouchableWithoutFeedback>
            <TextInput
              style={styles.input}
              color="grey"
              fontSize={30}
              onChangeText={value =>
                dispatch({ type: 'setInputValue', payload: value })
              }
            />
            <TouchableWithoutFeedback onPress={() => saveAudio()}>
              <View style={styles.saveButton}>
                <Text style={styles.textButton}>Save</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </Modal>
      <View style={styles.controls}>
        {renderButton('record-voice-over')}
        {!state.paused ? renderButton('pause') : renderButton('play-arrow')}
        {renderButton('stop')}
      </View>
      <Text style={styles.progressText}>{state.currentTime} s</Text>
      {/* <TouchableWithoutFeedback onPress={() => dispatch({type: 'toggleModal'})}>
        <View style={styles.saveButton}>
          <Text style={styles.textButton}>Save</Text>
        </View>
      </TouchableWithoutFeedback> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
  controls: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  progressText: {
    marginTop: 50,
    fontSize: 80,
    color: '#fff',
  },
  saveButton: {
    width: 200,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 50,
    borderRadius: 20,
  },
  textButton: {
    fontSize: 40,
    fontWeight: 'bold',
    color: 'grey',
  },
  backgroundModal: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalForm: {
    width: '90%',
    height: '47%',
    backgroundColor: 'white',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  input: {
    borderBottomWidth: 1,
    width: '70%',
  },
  close: {
    fontSize: 30,
  },
  navigate: {
    position: 'absolute',
    right: 0,
    top: 40,
  },
});

export default RecordingAudioScreen;
