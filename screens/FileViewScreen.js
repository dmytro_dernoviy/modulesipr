import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, View, TouchableOpacity } from 'react-native';
import FSService from '../services/FSService';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FilesComponent from '../components/FilesContainer';
import FileViewService from '../services/FileViewService';
import RNBackgroundDownloader from 'react-native-background-downloader';

const FileViewScreen = ({ navigation }) => {
  const [files, setFiles] = useState([]);

  console.log(RNBackgroundDownloader.directories.documents);

  useEffect(() => {
    FSService.getListOfDirectories().then(response => setFiles(response));
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate('AudioList')}>
          <Icon name="arrow-back" color="white" size={50} />
        </TouchableOpacity>
      </View>
      {files.map(file => (
        <TouchableOpacity
          style={styles.file}
          key={file.name}
          onPress={() => FileViewService.openByDocViewer(file.path, 'docx')}>
          <FilesComponent title={file.name} />
        </TouchableOpacity>
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'grey',
    flex: 1,
    color: 'black',
    alignItems: 'center',
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  file: {
    width: '90%',
    marginBottom: 10,
  },
});

export default FileViewScreen;
