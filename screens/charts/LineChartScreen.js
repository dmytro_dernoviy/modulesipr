import React, { useRef } from 'react';
import { TouchableOpacity, Text, Dimensions } from 'react-native';
import { LineChart } from 'react-native-charts-wrapper';
import ChartWrapper from '../../components/ChartWrapper';
import { useChartData } from './hooks';
import { lineChartData, lineChartSettings } from './chartsData';

const LineChartScreen = ({ navigation }) => {
  const { chartData, updateValues } = useChartData(lineChartData);
  const lineChart = useRef('Chart');

  return (
    <ChartWrapper navigation={navigation}>
      <LineChart
        ref={lineChart}
        data={chartData.data}
        xAxis={lineChartSettings.xAxis}
        yAxis={lineChartSettings.yAxis}
        style={{ height: Dimensions.get('window').height * 0.5 }}
        legend={{ enabled: false }}
        drawGridBackground={false}
        borderWidth={0}
        drawBorders={false}
        autoScaleMinMaxEnabled={true}
        touchEnabled={true}
        dragEnabled={false}
        scaleEnabled={false}
        scaleXEnabled={true}
        scaleYEnabled={true}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        highlightPerTapEnabled={true}
        highlightPerDragEnabled={false}
        dragDecelerationEnabled={true}
        dragDecelerationFrictionCoef={0.99}
        keepPositionOnRotation={false}
        animation={chartData.data.animation}
      />
      <TouchableOpacity onPress={updateValues}>
        <Text>Learn more</Text>
      </TouchableOpacity>
    </ChartWrapper>
  );
};

export default LineChartScreen;
