/* eslint-disable react-native/no-inline-styles */
import React, { useCallback } from 'react';
import { processColor } from 'react-native';
import { HorizontalBarChart } from 'react-native-charts-wrapper';
import ChartWrapper from '../../components/ChartWrapper';
import { useChartData } from './hooks';
import { horizontalBarChartData, horizontalBarYAxisData } from './chartsData';

const HorizontalBarChartScreen = ({ navigation }) => {
  const { chartData, horizontalBarXAxis } = useChartData(
    horizontalBarChartData,
  );
  const handleSelect = useCallback(event => {
    console.log(event.nativeEvent);
  }, []);

  return (
    <ChartWrapper navigation={navigation}>
      <HorizontalBarChart
        data={chartData}
        xAxis={horizontalBarXAxis}
        yAxis={horizontalBarYAxisData}
        style={{ flex: 1, backgroundColor: 'grey' }}
        gridBackgroundColor={processColor('#ffffff')}
        drawBarShadow={false}
        drawValueAboveBar={true}
        drawHighlightArrow={true}
        onSelect={handleSelect}
      />
    </ChartWrapper>
  );
};

export default HorizontalBarChartScreen;
