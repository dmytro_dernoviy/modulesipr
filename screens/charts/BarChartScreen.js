/* eslint-disable react-native/no-inline-styles */
import React, { useCallback } from 'react';
import { processColor } from 'react-native';
import { BarChart } from 'react-native-charts-wrapper';
import ChartWrapper from '../../components/ChartWrapper';
import { useChartData } from './hooks';
import { barChartData } from './chartsData';

const BarChartScreen = ({ navigation }) => {
  const { chartData, xAxis } = useChartData(barChartData);
  const handleSelect = useCallback(event => {
    console.log(event.nativeEvent);
  }, []);

  return (
    <ChartWrapper navigation={navigation}>
      <BarChart
        data={chartData}
        xAxis={xAxis}
        style={{ flex: 1 }}
        gridBackgroundColor={processColor('black')}
        animation={{ durationX: 5000 }}
        visibleRange={{ x: { min: 6, max: 6 } }}
        drawBarShadow={false}
        drawValueAboveBar={true}
        drawHighlightArrow={true}
        onSelect={handleSelect}
      />
    </ChartWrapper>
  );
};

export default BarChartScreen;
