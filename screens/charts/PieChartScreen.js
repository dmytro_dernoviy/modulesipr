/* eslint-disable react-native/no-inline-styles */
import React, { useCallback } from 'react';
import { processColor } from 'react-native';
import { PieChart } from 'react-native-charts-wrapper';
import ChartWrapper from '../../components/ChartWrapper';
import { useChartData } from './hooks';
import { pieChartData } from './chartsData';

const PieChartScreen = ({ navigation }) => {
  const { chartData } = useChartData(pieChartData);
  const handleSelect = useCallback(event => {
    console.log(event.nativeEvent);
  }, []);

  return (
    <ChartWrapper navigation={navigation}>
      <PieChart
        data={chartData}
        style={{ flex: 1 }}
        entryLabelColor={processColor('black')}
        entryLabelTextSize={15}
        drawEntryLabels={true}
        rotationEnabled={true}
        rotationAngle={0}
        usePercentValues={true}
        centerTextRadiusPercent={100}
        transparentCircleRadius={0}
        holeRadius={10}
        holeColor={processColor('#f0f0f0')}
        maxAngle={360}
        onSelect={handleSelect}
      />
    </ChartWrapper>
  );
};

export default PieChartScreen;
