import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, Dimensions } from 'react-native';
import { WebView } from 'react-native-webview';
import ChartWrapper from '../../components/ChartWrapper';
import { generateHTMLForChart } from '../../utils/generateHTMLForChart';

const { height } = Dimensions.get('window');

const HighChartScreen = ({ navigation }) => {
  const mockedValues = () => [
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
    Math.random() * 10,
  ];

  const [arrayOfValues, setArrayOfValues] = useState(mockedValues());
  const injectedHTML = {
    html: generateHTMLForChart(arrayOfValues),
  };

  return (
    <ChartWrapper navigation={navigation}>
      <WebView
        containerStyle={styles.webviewContainer}
        style={styles.webview}
        source={injectedHTML}
        javaScriptEnabled
        domStorageEnabled
        scrollEnabled={false}
        decelerationRate="normal"
        onError={syntheticEvent => {
          const { nativeEvent } = syntheticEvent;
          console.warn('WebView error: ', nativeEvent);
        }}
      />
      <TouchableOpacity onPress={() => setArrayOfValues(mockedValues())}>
        <Text>Update</Text>
      </TouchableOpacity>
    </ChartWrapper>
  );
};

const styles = StyleSheet.create({
  webviewContainer: {
    flex: 0,
    height: height * 0.5,
  },
  webview: {
    backgroundColor: 'transparent',
  },
});

export default HighChartScreen;
