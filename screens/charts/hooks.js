import { useState } from 'react';
import {
  barXAxisData,
  horizontalBarXAxisData,
  lineChartConfig,
  petrel,
  greenBlue,
} from './chartsData';
import update from 'immutability-helper';

export const useChartData = initialData => {
  const [chartData, setChartData] = useState(initialData);
  const [xAxis, setXAxis] = useState(barXAxisData);
  const [horizontalBarXAxis, setHorizontalBarAxis] = useState(
    horizontalBarXAxisData,
  );

  const updateValues = () => {
    setChartData(
      update(chartData, {
        data: {
          $set: {
            dataSets: [
              {
                values: [
                  { x: 1, y: Math.random() * 100 },
                  { x: 2, y: Math.random() * 100 },
                  { x: 3, y: Math.random() * 100 },
                  { x: 4, y: Math.random() * 100 },
                  { x: 5, y: Math.random() * 100 },
                  { x: 6, y: Math.random() * 100 },
                  { x: 7, y: Math.random() * 100 },
                  { x: 8, y: Math.random() * 100 },
                  { x: 9, y: Math.random() * 100 },
                  { x: 10, y: Math.random() * 100 },
                  { x: 11, y: Math.random() * 100 },
                ],
                label: 'A',
                config: lineChartConfig(petrel, greenBlue),
              },
            ],
            animation: {
              durationX: 0,
              durationY: 500,
              easingX: 'EaseInCirc',
              random: Math.random(),
            },
          },
        },
      }),
    );
  };

  return {
    chartData,
    xAxis,
    setXAxis,
    horizontalBarXAxis,
    setHorizontalBarAxis,
    updateValues,
  };
};
