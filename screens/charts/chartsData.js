import { processColor } from 'react-native';

export const greenBlue = 'rgba(26, 182, 151, 0.5)';
export const petrel = 'rgba(59, 145, 153, 0.1)';

export const lineChartConfig = (color1, color2) => {
  return {
    mode: 'CUBIC_BEZIER',
    drawValues: false,
    lineWidth: 2,
    drawCircles: false,
    circleColor: processColor(color1),
    drawCircleHole: false,
    circleRadius: 5,
    highlightColor: processColor('transparent'),
    color: processColor('bluesky'),
    drawFilled: true,
    fillGradient: {
      colors: [processColor(color1), processColor(color2)],
      positions: [0, 0.5],
      angle: 90,
      orientation: 'BOTTOM_TOP',
    },
    fillAlpha: 1000,
    valueTextSize: 15,
  };
};

export const lineChartSettings = {
  xAxis: {
    // granularityEnabled: true,
    // granularity: 1,
    drawGridLines: true,
    drawLabels: false,
    gridColor: processColor('rgba(59, 145, 153, 0.15)'),
    gridLineWidth: 2,
    axisLineWidth: 0,
  },
  yAxis: {
    left: {
      enabled: false,
    },
    right: {
      drawGridLines: false,
      axisLineWidth: 0,
      enabled: true,
      position: 'OUTSIDE_CHART',
      textColor: processColor('white'),
      textSize: 20,
      fontWeight: 'bold',
    },
  },
  marker: {
    enabled: true,
    digits: 2,
    backgroundTint: processColor('teal'),
    markerColor: processColor('#F0C0FF8C'),
    textColor: processColor('white'),
  },
  animation: {
    durationX: 0,
    durationY: 500,
    easingX: 'EaseInCirc',
    random: Math.random(),
  },
};

export const lineChartData = {
  data: {
    dataSets: [
      {
        values: [
          { x: 1, y: Math.random() * 100 },
          { x: 2, y: Math.random() * 100 },
          { x: 3, y: Math.random() * 100 },
          { x: 4, y: Math.random() * 100 },
          { x: 5, y: Math.random() * 100 },
          { x: 6, y: Math.random() * 100 },
          { x: 7, y: Math.random() * 100 },
          { x: 8, y: Math.random() * 100 },
          { x: 9, y: Math.random() * 100 },
          { x: 10, y: Math.random() * 100 },
          { x: 11, y: Math.random() * 100 },
        ],
        label: 'A',
        config: lineChartConfig(petrel, greenBlue),
      },
    ],
    animation: lineChartSettings.animation,
  },
};

export const pieChartData = {
  dataSets: [
    {
      values: [
        { value: 45, label: 'Sandwiches' },
        { value: 21, label: 'Salads' },
        { value: 15, label: 'Soup' },
        { value: 4, label: 'Beverages' },
        { value: 15, label: 'Desserts' },
      ],
      label: 'Pie dataset',
      config: {
        colors: [
          processColor('#C0FF8C'),
          processColor('#FFF78C'),
          processColor('#FFD08C'),
          processColor('#8CEAFF'),
          processColor('#FF8C9D'),
        ],
        valueTextSize: 20,
        valueTextColor: processColor('black'),
        sliceSpace: 3,
        selectionShift: 20,
        valueFormatter: "#.#'%'",
        valueLineColor: processColor('black'),
        valueLinePart1Length: 0.5,
      },
    },
  ],
};

export const barChartData = {
  dataSets: [
    {
      values: [
        { y: 100 },
        { y: 105 },
        { y: 102 },
        { y: 110 },
        { y: 114 },
        { y: 109 },
        { y: 105 },
        { y: 99 },
        { y: 95 },
      ],
      label: 'Bar dataSet',
      config: {
        color: processColor('lightgreen'),
        barShadowColor: processColor('black'),
        highlightAlpha: 90,
        highlightColor: processColor('black'),
      },
    },
  ],
  config: {
    barWidth: 0.9,
  },
};

export const barXAxisData = {
  valueFormatter: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
  ],
  granularityEnabled: true,
  gridColor: processColor('black'),
  lineColor: processColor('red'),
  gridLineWidth: 1,
  granularity: 1,
};

export const horizontalBarChartData = {
  dataSets: [
    {
      values: [
        { y: 77 },
        { y: 105 },
        { y: 32 },
        { y: 110 },
        { y: 92 },
        { y: 52 },
        { y: 105 },
        { y: 65 },
        { y: 10 },
      ],
      label: 'Bar dataSet',
      config: {
        color: processColor('lightgreen'),
        barShadowColor: processColor('lightgrey'),
        highlightAlpha: 90,
        highlightColor: processColor('red'),
      },
    },
  ],
};

export const horizontalBarXAxisData = {
  valueFormatter: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
  ],
  position: 'BOTTOM_INSIDE',
  granularityEnabled: true,
  granularity: 1,
  labelCount: 10,
  gridColor: processColor('black'),
};

export const horizontalBarYAxisData = { left: { axisMinimum: 0 } };
