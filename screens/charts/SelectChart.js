import React from 'react';
import { TouchableOpacity, StyleSheet, Text, SafeAreaView } from 'react-native';

const SelectChart = ({ navigation }) => {
  const intialState = [
    'LineChart',
    'PieChart',
    'BarChart',
    'HorizontalBarChart',
    'HighChart',
  ];

  return (
    <SafeAreaView style={styles.container}>
      {intialState.map(chartName => (
        <TouchableOpacity
          style={styles.chartItem}
          key={chartName}
          onPress={() => navigation.navigate(chartName)}>
          <Text style={styles.text}>{chartName}</Text>
        </TouchableOpacity>
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    alignItems: 'center',
  },
  chartItem: {
    width: '80%',
    padding: 20,
    borderWidth: 2,
    borderRadius: 20,
    backgroundColor: 'lightgreen',
    alignItems: 'center',
    marginTop: 20,
  },
  text: {
    fontSize: 20,
  },
});

export default SelectChart;
