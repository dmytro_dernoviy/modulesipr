import React, { useEffect, useState, useRef, useCallback } from 'react';
import { StyleSheet, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import getProductsActions from '../redux/getProducts/actionCreators';
import Button from '../components/Button';

const useGetProducts = () => {
  const products = useSelector(state => state.getProductsReducer.products);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProductsActions.request());
  }, [dispatch]);

  return products;
};

const HooksTraining = () => {
  const [count, setCount] = useState(0);
  const buttonEl = useRef('Button');

  useEffect(() => {
    consoleCount();
  }, [consoleCount, count]);

  const consoleCount = useCallback(() => {
    if (count !== 4) {
      console.log(count);
    }
  }, [count]);

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={() => setCount(count + 1)}>
        <Text>{count}</Text>
      </TouchableOpacity>
      <Button />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default HooksTraining;
