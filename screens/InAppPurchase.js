import React, { useEffect, useCallback } from 'react';
import * as RNIap from 'react-native-iap';
import { SafeAreaView, StyleSheet, Platform } from 'react-native';

const InAppPurchase = () => {
  const itemSkus = Platform.select({
    ios: ['org.reactjs.native.example.modulesIPR.testpurchase'],
    android: ['org.reactjs.native.example.modulesIPR.testpurchase'],
  });

  useEffect(() => {
    getProducts();
  }, [getProducts, itemSkus]);

  const getProducts = useCallback(async () => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      console.log(products);
    } catch (error) {
      console.log(error);
    }
  }, [itemSkus]);

  return <SafeAreaView style={styles.container} />;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'grey',
    flex: 1,
    color: 'black',
  },
});

export default InAppPurchase;
